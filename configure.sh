#!/bin/bash

  # This script is intended to create the database 'status' for the probes included in Inframon (RHEL Version)
  # You can manually edit the MySQL credentials and then answer 'Y' or 'Yes' when prompted asking if you have already configured these.
  # Make sure your db user has read/write permissions for this installation.

  #System Stuff
  dbuser="username"
  dbpass="password"

  while true; do
          read -p "Have you already configured your database credentials in the script? (Y/n): " yn
          case $yn in
                  [Yy]* ) break;;
                  [Nn]* ) read -p "Please enter a user for MySQL / MariaDB: " dbuser ; echo '' ; read -s -p "Please enter $dbuser's password: " dbpass ;break;;
          * ) echo "Please answer yes or no.";;
          esac
  done

  echo ''
  echo "The database installation will be using $dbuser's account"
  echo ''
  echo 'We will now proceed with creating the Secsuite database for the Inframon probes. Your credentials will appear below once done.'
  echo ''

  mysql --user="$dbuser" --password="$dbpass" -e "DROP DATABASE status;"

  mysql --user="$dbuser" --password="$dbpass" -e "CREATE DATABASE IF NOT EXISTS status;"

  mysqlshow --user="$dbuser" --password="$dbpass" status >> $homedir/dbfile.data

  if grep -q "status" "$homedir/dbfile.data"; then
        echo "The Inframon database has been configured correctly. Proceeding with install..." ; rm $homedir/dbfile.data
  else
        echo "There was an error creating the database. Exiting." ; rm $homedir/dbfile.data ; exit
  fi

  #Create httpd tables;
  mysql --user="$dbuser" --password="$dbpass" -e "USE status; CREATE TABLE IF NOT EXISTS httpd_status (id INT AUTO_INCREMENT NOT NULL, hostname VARCHAR(90) NOT NULL, httpdstatus VARCHAR(90) NOT NULL, unixtime INT NOT NULL, PRIMARY KEY(id));"
  mysql --user="$dbuser" --password="$dbpass" -e "USE status; CREATE TABLE IF NOT EXISTS hist_httpd_status (id INT AUTO_INCREMENT NOT NULL, hostname VARCHAR(90) NOT NULL, httpdstatus VARCHAR(90) NOT NULL, unixtime INT NOT NULL, PRIMARY KEY(id));"

  #Create bandwidth tables;
  mysql --user="$dbuser" --password="$dbpass" -e "USE status; CREATE TABLE IF NOT EXISTS bandwidth_usage (id INT AUTO_INCREMENT NOT NULL, type VARCHAR(5) NOT NULL, packets INT NOT NULL, bytes BIGINT NOT NULL, humanformat VARCHAR(10) NOT NULL, unixtime INT NOT NULL, PRIMARY KEY(id));"
  mysql --user="$dbuser" --password="$dbpass" -e "USE status; CREATE TABLE IF NOT EXISTS hist_bandwidth_usage (id INT AUTO_INCREMENT NOT NULL, type VARCHAR(5) NOT NULL, packets INT NOT NULL, bytes BIGINT NOT NULL, humanformat VARCHAR(10) NOT NULL, unixtime INT NOT NULL, PRIMARY KEY(id));"

  #Create load average tables;
  mysql --user="$dbuser" --password="$dbpass" -e "USE status; CREATE TABLE IF NOT EXISTS cpuloadavg_stats (id INT AUTO_INCREMENT NOT NULL, hostname VARCHAR(90) NOT NULL, loadonemin VARCHAR(5) NOT NULL, loadtenmin VARCHAR(5) NOT NULL, loadfifteenmin VARCHAR(5) NOT NULL, unixtime INT NOT NULL, PRIMARY KEY(id));"
  mysql --user="$dbuser" --password="$dbpass" -e "USE status; CREATE TABLE IF NOT EXISTS hist_cpuloadavg_stats (id INT AUTO_INCREMENT NOT NULL, hostname VARCHAR(90) NOT NULL, loadonemin VARCHAR(5) NOT NULL, loadtenmin VARCHAR(5) NOT NULL, loadfifteenmin VARCHAR(5) NOT NULL, unixtime INT NOT NULL, PRIMARY KEY(id));"

  #Create latency tables;
  mysql --user="$dbuser" --password="$dbpass" -e "USE status; CREATE TABLE IF NOT EXISTS latency_stats (id INT AUTO_INCREMENT NOT NULL, hostname VARCHAR(90) NOT NULL, ipaddr VARCHAR(20) NOT NULL, port INT NOT NULL, latency VARCHAR(30) NOT NULL, unixtime INT NOT NULL, PRIMARY KEY(id));"
  mysql --user="$dbuser" --password="$dbpass" -e "USE status; CREATE TABLE IF NOT EXISTS hist_latency_stats (id INT AUTO_INCREMENT NOT NULL, hostname VARCHAR(90) NOT NULL, ipaddr VARCHAR(20) NOT NULL, port INT NOT NULL, latency VARCHAR(30) NOT NULL, unixtime INT NOT NULL, PRIMARY KEY(id));"

  #Create disk tables;
  mysql --user="$dbuser" --password="$dbpass" -e "USE status; CREATE TABLE IF NOT EXISTS disk_stats (id INT AUTO_INCREMENT NOT NULL, hostname VARCHAR(90) NOT NULL, filesystem VARCHAR(15) NOT NULL, size VARCHAR(5) NOT NULL, used VARCHAR(5) NOT NULL, available VARCHAR(5) NOT NULL, usedpercent VARCHAR(5) NOT NULL, mountpoint VARCHAR(5) NOT NULL, unixtime INT NOT NULL, PRIMARY KEY(id));"
  mysql --user="$dbuser" --password="$dbpass" -e "USE status; CREATE TABLE IF NOT EXISTS hist_disk_stats (id INT AUTO_INCREMENT NOT NULL, hostname VARCHAR(90) NOT NULL, filesystem VARCHAR(15) NOT NULL, size VARCHAR(5) NOT NULL, used VARCHAR(5) NOT NULL, available VARCHAR(5) NOT NULL, usedpercent VARCHAR(5) NOT NULL, mountpoint VARCHAR(5) NOT NULL, unixtime INT NOT NULL, PRIMARY KEY(id));"

  #Create memory tables;
  mysql --user="$dbuser" --password="$dbpass" -e "USE status; CREATE TABLE IF NOT EXISTS memory_stats (id INT AUTO_INCREMENT NOT NULL, hostname VARCHAR(90) NOT NULL, type VARCHAR(5) NOT NULL, total VARCHAR(5) NOT NULL, used VARCHAR(5) NOT NULL, free VARCHAR(5) NOT NULL, shared VARCHAR(5) NOT NULL, cached VARCHAR(5) NOT NULL, available VARCHAR(5) NOT NULL, unixtime INT NOT NULL, PRIMARY KEY(id));"
  mysql --user="$dbuser" --password="$dbpass" -e "USE status; CREATE TABLE IF NOT EXISTS hist_memory_stats (id INT AUTO_INCREMENT NOT NULL, hostname VARCHAR(90) NOT NULL, type VARCHAR(5) NOT NULL, total VARCHAR(5) NOT NULL, used VARCHAR(5) NOT NULL, free VARCHAR(5) NOT NULL, shared VARCHAR(5) NOT NULL, cached VARCHAR(5) NOT NULL, available VARCHAR(5) NOT NULL, unixtime INT NOT NULL, PRIMARY KEY(id));"

  #Create user tables;
  mysql --user="$dbuser" --password="$dbpass" -e "USE status; CREATE TABLE IF NOT EXISTS user_stats (id INT AUTO_INCREMENT NOT NULL, hostname VARCHAR(90) NOT NULL, user VARCHAR(20) NOT NULL, pts VARCHAR(5) NOT NULL, datelogged VARCHAR(15) NOT NULL, timelogged VARCHAR(6) NOT NULL, ipaddr VARCHAR(25) NOT NULL, unixtime INT NOT NULL, PRIMARY KEY(id));"
  mysql --user="$dbuser" --password="$dbpass" -e "USE status; CREATE TABLE IF NOT EXISTS hist_user_stats (id INT AUTO_INCREMENT NOT NULL, hostname VARCHAR(90) NOT NULL, user VARCHAR(20) NOT NULL, pts VARCHAR(5) NOT NULL, datelogged VARCHAR(15) NOT NULL, timelogged VARCHAR(6) NOT NULL, ipaddr VARCHAR(25) NOT NULL, unixtime INT NOT NULL, PRIMARY KEY(id));"
