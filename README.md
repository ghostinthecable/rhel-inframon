# rhel-inframon
This repository is dedicated to Inframon (from Secsuite), which has been ported over for RHEL hosts compatibility.

## Installation guide
1) Please make sure that once you have cloned this repo, the directory 'inframon' should be placed at '/root/scripts/SECSUITE/inframon'.

2) Ensure to edit the variables "dbuser" & "dbpass" in the script configure.sh, then execute it to create the database & tables.

3) Once all is in place, you will need to edit the variables "dbuser" & "dbpass" according to your credentials on your system(s) in each of the following scripts:
- /root/scripts/SECSUITE/inframon/bandwidth/bandwidth-monitor-local.sh
- /root/scripts/SECSUITE/inframon/disks/disk-monitor-local.sh
- /root/scripts/SECSUITE/inframon/httpd/httpd-monitor-local.sh
- /root/scripts/SECSUITE/inframon/latency/latency-monitor-local.sh
- /root/scripts/SECSUITE/inframon/loadavg/cpu-load-monitor-local.sh
- /root/scripts/SECSUITE/inframon/memory/memory-monitor-local.sh
- /root/scripts/SECSUITE/inframon/users/users-monitor-local.sh

### Disk Monitor Config
Within the directory /root/scripts/SECSUITE/inframon/disks/ you will need to edit the file "disks.conf" to include the name of the filesystem which you would like to monitor, you may monitor multiple filesystems by adding them in one per line.

### Latency Monitor Config
Within the directory /root/scripts/SECSUITE/inframon/latency/ you will need to edit the file "hosts.conf", the first column is the IP/FQDN, the second column is the port.
Example:  
```
127.0.0.1       80  
127.0.0.1       22
```
  
## Automation
Once complete, you may install the probes in your crontab with the following lines:  
```
* * * * * sudo bash /root/scripts/SECSUITE/inframon/bandwidth/bandwidth-monitor-local.sh
* * * * * sudo bash /root/scripts/SECSUITE/inframon/disks/disk-monitor-local.sh
* * * * * sudo bash /root/scripts/SECSUITE/inframon/httpd/httpd-monitor-local.sh
* * * * * sudo bash /root/scripts/SECSUITE/inframon/latency/latency-monitor-local.sh
* * * * * sudo bash /root/scripts/SECSUITE/inframon/loadavg/cpu-load-monitor-local.sh
* * * * * sudo bash /root/scripts/SECSUITE/inframon/memory/memory-monitor-local.sh
* * * * * sudo bash /root/scripts/SECSUITE/inframon/users/users-monitor-local.sh
```
