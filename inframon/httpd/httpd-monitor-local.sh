#!/bin/bash

  #Environment
  homedir="/root/scripts/SECSUITE/inframon"
  probedir="$homedir/httpd"

  #DB Credentials;
  dbuser="username"
  dbpass="password"

  #Begin Probe
  systemctl status httpd >> $probedir/statusfile.data

  if grep -q "Active: active" "$probedir/statusfile.data"; then
        cat $probedir/statusfile.data | grep -E "Active: active" > $probedir/httpdactiveinfo.data
  fi
  if grep -q "Active: inactive" "$probedir/statusfile.data"; then
        cat $probedir/statusfile.data | grep -E "Active: inactive" > $probedir/httpddowninfo.data
  fi
  rm -rf $probedir/statusfile.data

  echo "$(hostname)," >> $probedir/buildfile.data
  if [ -f "$probedir/httpdactiveinfo.data" ]; then
        echo "$(cat $probedir/httpdactiveinfo.data)," >> $probedir/buildfile.data
        rm -rf $probedir/httpdactiveinfo.data
  fi
  if [ -f "$probedir/httpddowninfo.data" ]; then
        echo "$(cat $probedir/httpddowninfo.data)," >> $probedir/buildfile.data
        rm -rf $probedir/httpddowninfo.data
  fi

  date +%s >> $probedir/buildfile.data
  tr -d "\n" < $probedir/buildfile.data > $probedir/httpdimport.csv
  sed -i "s/   //g" $probedir/httpdimport.csv

  mysql --user="$dbuser" --password="$dbpass" -e "USE status; TRUNCATE TABLE httpd_status;"

  mysql --user="$dbuser" --password="$dbpass" -e "USE status; LOAD DATA LOCAL INFILE '$probedir/httpdimport.csv' INTO TABLE httpd_status FIELDS TERMINATED BY ',' LINES TERMINATED BY '\r\n' (hostname, httpdstatus, unixtime)"

  mysql --user="$dbuser" --password="$dbpass" -e "USE status; INSERT INTO hist_httpd_status (hostname, httpdstatus, unixtime) SELECT hostname, httpdstatus, unixtime FROM httpd_status;"

  rm -rf $probedir/httpdimport.csv $probedir/buildfile.data

  exit
