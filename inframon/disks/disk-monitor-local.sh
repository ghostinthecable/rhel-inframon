#!/bin/bash

  #Environment
  homedir="/root/scripts/SECSUITE/inframon"
  probedir="$homedir/disks"

  #DB Credentials
  dbuser="username"
  dbpass="password"

  #Get disks
  if [ -f "$probedir/disks.conf" ]; then
        diskfile="$probedir/disks.conf"
  fi

  #Begin probe
  i="0"
  while IFS= read -r line; do
  (( i++ ))
        echo "$(hostname)," >> $probedir/diskoutput.data
        df -h | grep -E "$line" | awk '{print $1 "," $2 "," $3 "," $4 "," $5 "," $6}' >> $probedir/diskoutput.data
        echo ",$(date +%s)" >> $probedir/diskoutput.data
        tr -d "\n" < $probedir/diskoutput.data > $probedir/$i.yeet
        echo '' >> $probedir/$i.yeet
        rm -rf $probedir/diskoutput.data
  done <$probedir/disks.conf


  cat $probedir/*.yeet* >> $probedir/disklist.csv

  cat $probedir/disklist.csv


  mysql --user="$dbuser" --password="$dbpass" -e "USE status; TRUNCATE TABLE disk_stats;"
  mysql --user="$dbuser" --password="$dbpass" -e "USE status; LOAD DATA LOCAL INFILE '$probedir/disklist.csv' INTO TABLE disk_stats FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n' (hostname, filesystem, size, used, available, usedpercent, mountpoint, unixtime);"
  mysql --user="$dbuser" --password="$dbpass" -e "USE status; INSERT INTO hist_disk_stats (hostname, filesystem, size, used, available, usedpercent, mountpoint, unixtime) SELECT hostname, filesystem, size, used, available, usedpercent, mountpoint, unixtime FROM disk_stats;"

  rm -rf $probedir/diskform.data $probedir/disklist.csv $probedir/*.yeet*

  exit
