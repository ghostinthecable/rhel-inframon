#!/bin/bash

  #Environment
  homedir="/root/scripts/SECSUITE/inframon"
  probedir="$homedir/bandwidth"

  #DB Credentials;
  dbuser="username"
  dbpass="password"

  #Interface to monitor (Change manually for your environment);
  interface="ens33"

  #Begin Probe
  ifconfig >> $probedir/netstats.data
  cat $probedir/netstats.data | grep -A7 "$interface" >> $probedir/intface.data

  cat $probedir/intface.data | grep -E "RX" >> $probedir/intfaceRX.data
  cat $probedir/intface.data | grep -E "TX" >> $probedir/intfaceTX.data

  cat $probedir/intfaceRX.data | grep -E "bytes" >> $probedir/intfaceRXinfo.data
  cat $probedir/intfaceTX.data | grep -E "bytes" >> $probedir/intfaceTXinfo.data

  sed -i 's/RX packets//g' $probedir/intfaceRXinfo.data
  sed -i 's/TX packets//g' $probedir/intfaceTXinfo.data
  sed -i 's/        //g' $probedir/intfaceRXinfo.data
  sed -i 's/        //g' $probedir/intfaceTXinfo.data
  sed -i 's/bytes/\,/g' $probedir/intfaceRXinfo.data
  sed -i 's/bytes/\,/g' $probedir/intfaceTXinfo.data
  sed -i 's/(/\,/g' $probedir/intfaceRXinfo.data
  sed -i 's/(/\,/g' $probedir/intfaceTXinfo.data
  sed -i 's/iB)//g' $probedir/intfaceRXinfo.data
  sed -i 's/iB)//g' $probedir/intfaceTXinfo.data
  sed -i 's/ //g' $probedir/intfaceRXinfo.data
  sed -i 's/ //g' $probedir/intfaceTXinfo.data

  sed -i '1 i\RX\,' $probedir/intfaceRXinfo.data
  sed -i '1 i\TX\,' $probedir/intfaceTXinfo.data

  echo ",$(date +%s)" >> $probedir/intfaceRXinfo.data
  echo ",$(date +%s)" >> $probedir/intfaceTXinfo.data

  tr -d "\n" < $probedir/intfaceRXinfo.data > $probedir/intfaceRXimport.csv
  tr -d "\n" < $probedir/intfaceTXinfo.data > $probedir/intfaceTXimport.csv

  echo '' >> $probedir/intfaceRXimport.csv
  echo '' >> $probedir/intfaceTXimport.csv

  mysql --user="$dbuser" --password="$dbpass" -e "USE status; TRUNCATE TABLE bandwidth_usage;"
  mysql --user="$dbuser" --password="$dbpass" -e "USE status; LOAD DATA LOCAL INFILE '$probedir/intfaceRXimport.csv' INTO TABLE bandwidth_usage FIELDS TERMINATED BY ',' LINES TERMINATED BY '\r\n' (type, packets, bytes, humanformat, unixtime)"
  mysql --user="$dbuser" --password="$dbpass" -e "USE status; LOAD DATA LOCAL INFILE '$probedir/intfaceTXimport.csv' INTO TABLE bandwidth_usage FIELDS TERMINATED BY ',' LINES TERMINATED BY '\r\n' (type, packets, bytes, humanformat, unixtime)"
  mysql --user="$dbuser" --password="$dbpass" -e "USE status; INSERT INTO hist_bandwidth_usage (type, packets, bytes, humanformat, unixtime) SELECT type, packets, bytes, humanformat, unixtime FROM bandwidth_usage;"

  rm $probedir/netstats.data $probedir/intface.data $probedir/intfaceRX.data $probedir/intfaceTX.data $probedir/intfaceRXinfo.data $probedir/intfaceTXinfo.data $probedir/intfaceRXimport.csv $probedir/intfaceTXimport.csv

  exit
