#!/bin/bash

  #Environment
  homedir="/root/scripts/SECSUITE/inframon"
  probedir="$homedir/memory"

  #DB Credentials
  dbuser="username"
  dbpass="password"

  #Begin probe
  free -m -h | awk '{print $1 "," $2 "," $3 "," $4 "," $5 "," $6 "," $7}' | tail -n +2 >> $probedir/memfile.data
  sed -i 's/\://g' $probedir/memfile.data
  cat $probedir/memfile.data >> $probedir/memfile.csv

  hn="$(hostname)"
  ts="$(date +%s)"

  sed -i "s/^/$hn\,/" $probedir/memfile.csv
  sed -i "s/$/\,$ts/" $probedir/memfile.csv


  mysql --user="$dbuser" --password="$dbpass" -e "USE status; TRUNCATE TABLE memory_stats;"
  mysql --user="$dbuser" --password="$dbpass" -e "USE status; LOAD DATA LOCAL INFILE '$probedir/memfile.csv' INTO TABLE memory_stats FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n' (hostname, type, total, used, free, shared, cached, available, unixtime);"
  mysql --user="$dbuser" --password="$dbpass" -e "USE status; INSERT INTO hist_memory_stats (hostname, type, total, used, free, shared, cached, available, unixtime) SELECT hostname, type, total, used, free, shared, cached, available, unixtime FROM memory_stats;"

  rm -rf $probedir/memfile.data $probedir/memfile.csv

  exit
