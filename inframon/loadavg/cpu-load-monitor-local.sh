#!/bin/bash

  #Environment
  homedir="/root/scripts/SECSUITE/inframon"
  probedir="$homedir/loadavg"

  #DB Credentials;
  dbuser="username"
  dbpass="password"

  #Begin Probe
  echo "$(hostname)," >> $probedir/loadavg.data
  cat /proc/loadavg | awk '{print $1 "," $2 "," $3}' >> $probedir/loadavg.data
  echo ",$(date +%s)" >> $probedir/loadavg.data

  tr -d "\n" < $probedir/loadavg.data > $probedir/loadavgimport.csv

  echo '' >> $probedir/loadavgimport.csv

  mysql --user="$dbuser" --password="$dbpass" -e "USE status; TRUNCATE TABLE cpuloadavg_stats;"
  mysql --user="$dbuser" --password="$dbpass" -e "USE status; LOAD DATA LOCAL INFILE '$probedir/loadavgimport.csv' INTO TABLE cpuloadavg_stats FIELDS TERMINATED BY ',' LINES TERMINATED BY '\r\n' (hostname, loadonemin, loadtenmin, loadfifteenmin, unixtime);"
  mysql --user="$dbuser" --password="$dbpass" -e "USE status; INSERT INTO hist_cpuloadavg_stats (hostname, loadonemin, loadtenmin, loadfifteenmin, unixtime) SELECT hostname, loadonemin, loadtenmin, loadfifteenmin, unixtime FROM cpuloadavg_stats;"

  rm -rf $probedir/loadavg.data $probedir/loadavgimport.csv

  exit
