#!/bin/bash

  #Environment
  homedir="/root/scripts/SECSUITE/inframon"
  probedir="$homedir/users"

  #DB Credentials;
  dbuser="username"
  dbpass="password"

  #Begin probe
  who | awk '{print $1 "," $2 "," $3 "," $4 "," $5}' >> $probedir/whofile.data
  sed -i 's/(//g' $probedir/whofile.data
  sed -i 's/)//g' $probedir/whofile.data
  hn="$(hostname)"
  ts="$(date +%s)"
  sed -i "s/^/$hn\,/" $probedir/whofile.data
  sed -i "s/$/\,$ts/" $probedir/whofile.data

  cat $probedir/whofile.data >> $probedir/whofile.csv

  mysql --user="$dbuser" --password="$dbpass" -e "USE status; TRUNCATE TABLE user_stats;"
  mysql --user="$dbuser" --password="$dbpass" -e "USE status; LOAD DATA LOCAL INFILE '$probedir/whofile.csv' INTO TABLE user_stats FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n' (hostname, user, pts, datelogged, timelogged, ipaddr, unixtime);"
  mysql --user="$dbuser" --password="$dbpass" -e "USE status; INSERT INTO hist_user_stats (hostname, user, pts, datelogged, timelogged, ipaddr, unixtime) SELECT hostname, user, pts, datelogged, timelogged, ipaddr, unixtime FROM user_stats;"

  rm -rf $probedir/whofile.data $probedir/whofile.csv

  exit
