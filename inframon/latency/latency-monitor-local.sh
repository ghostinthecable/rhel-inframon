#!/bin/bash

  #Environment
  homedir="/root/scripts/SECSUITE/inframon"
  probedir="$homedir/latency"

  #DB Credentials
  dbuser="username"
  dbpass="password"

  #Get hosts
  if [ -f "$probedir/hosts.conf" ]; then
        hostsfile="$probedir/hosts.conf"
  fi

  #Begin Probe
  while IFS= read -r line; do
        host="$(echo $line | awk '{print $1}')"
        port="$(echo $line | awk '{print $2}')"
        nmap -sS $host -p$port 2>/dev/null | grep -E "scanned in" >> $port.data
        sed -i 's/Nmap done: 1 IP address (1 host up) scanned in //g' $port.data
        printf "$(hostname), $host, $port, $(cat $port.data), $(date +%s)\n" >> $probedir/importfile.csv
        rm $port.data
  done < $hostsfile

  echo "Import file data:"
  cat $probedir/importfile.csv

  mysql --user="$dbuser" --password="$dbpass" -e "USE status; TRUNCATE TABLE latency_stats;"
  mysql --user="$dbuser" --password="$dbpass" -e "USE status; LOAD DATA LOCAL INFILE '$probedir/importfile.csv' INTO TABLE latency_stats FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n' (hostname, ipaddr, port, latency, unixtime)"
  mysql --user="$dbuser" --password="$dbpass" -e "USE status; INSERT INTO hist_latency_stats (hostname, ipaddr, port, latency, unixtime) SELECT hostname, ipaddr, port, latency, unixtime FROM latency_stats;"

  rm -rf $port.data $probedir/importfile.csv

  exit
